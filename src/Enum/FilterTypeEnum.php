<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Enum;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\SearchCustomFieldFilter;
use Dexodus\EntityTableBundle\Exception\NotFoundFilterTypeException;

enum FilterTypeEnum: string
{
    case TYPE_ENUM_SEARCH = 'enum_search';
    case TYPE_ASYNC_SEARCH = 'async_search';
    case TYPE_SEARCH = 'search';
    case TYPE_SORT = 'sort';

    public static function mapFilter(string $filterClass): FilterTypeEnum
    {
        switch ($filterClass) {
            case SearchFilter::class:
            case SearchCustomFieldFilter::class:
                return self::TYPE_SEARCH;
            case OrderFilter::class:
                return self::TYPE_SORT;
        }

        throw new NotFoundFilterTypeException();
    }
}
