<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Service;

use Dexodus\EntityFormBundle\Service\PathsGenerator;
use Dexodus\EntityTableBundle\Dto\EntityTableStructure;

class EntityTableStructureGenerator implements EntityTableStructureGeneratorInterface
{
    public function __construct(
        private readonly EntityTableLoaderInterface $entityTableLoader,
        private readonly ColumnGenerator $columnGenerator,
        private readonly PathsGenerator $pathsGenerator,
        private readonly EntityFilterExtractor $entityFilterExtractor,
        private readonly ActionsExtractor $actionsExtractor,
    ) {
    }

    public function generate(string $entityTableName): EntityTableStructure
    {
        $entityTable = $this->entityTableLoader->get($entityTableName);

        $entityTableStructure = new EntityTableStructure();
        $entityTableStructure->name = $entityTable->name;
        $entityTableStructure->entity = $entityTable->entity;
        $entityTableStructure->columns = $this->columnGenerator->generate($entityTable->entity);

        $paths = $this->pathsGenerator->generateForResource($entityTable->entity, ['collection']);
        $entityTableStructure->path = $entityTable->attribute->path ?? $paths['collection'];
        $entityTableStructure->actions = $this->actionsExtractor->getActions($entityTable);
        $this->entityFilterExtractor->fillFiltersInStructure($entityTableStructure);

        return $entityTableStructure;
    }
}
