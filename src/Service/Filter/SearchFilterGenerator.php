<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Service\Filter;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Dexodus\EntityFormBundle\Service\PathsGenerator;
use Dexodus\EntityTableBundle\Attribute\FilterGenerator;
use Dexodus\EntityTableBundle\Dto\Filter;
use Dexodus\EntityTableBundle\Enum\FilterTypeEnum;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

#[FilterGenerator(SearchFilter::class)]
class SearchFilterGenerator implements FilterGeneratorInterface
{
    public function __construct(
        private PropertyInfoExtractorInterface $propertyInfoExtractor,
        private PathsGenerator $pathsGenerator,
    ) {
    }

    public function generate(string $class, string $property): Filter
    {
        $filter = new Filter();

        $filter->type = FilterTypeEnum::TYPE_SEARCH;
        $filter->query = "{$property}[]=";

        $types = $this->propertyInfoExtractor->getTypes($class, $property);
        $type = $types[0];

        if ($type->getBuiltinType() === 'object') {
            $paths = $this->pathsGenerator->generateForResource($type->getClassName(), ['collection']);

            if (!empty($paths)) {
                $filter->options = [
                    'url' => $paths['collection'],
                ];
                $filter->type = FilterTypeEnum::TYPE_ASYNC_SEARCH;
            }
        }

        return $filter;
    }
}
