<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Service\Filter;

use App\Filter\SearchCustomFieldFilter;
use Dexodus\EntityTableBundle\Attribute\FilterGenerator;
use Dexodus\EntityTableBundle\Dto\Filter;

#[FilterGenerator(SearchCustomFieldFilter::class)]
class SearchCustomFieldFilterGenerator implements FilterGeneratorInterface
{
    public function generate(string $class, string $property): Filter
    {
        $filter = new Filter();
        $filter->query = "{$property}=";

        return $filter;
    }
}
