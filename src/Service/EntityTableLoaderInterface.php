<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Service;

use Dexodus\EntityTableBundle\Dto\EntityTable;

interface EntityTableLoaderInterface
{
    /** @param string[] $entityTables */
    public function setEntityTables(array $entityTables): void;

    public function get(string $entityTableName): EntityTable;
    public function has(string $entityTableName): bool;

    /** @return EntityTable[] */
    public function getAll(): array;

    public function convertClassToEntityFormName(string $class): string;
}
