<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Service;

use Dexodus\EntityFormBundle\Attribute\FieldAttributeInterface;
use Dexodus\EntityFormBundle\Attribute\Priority;
use Dexodus\EntityFormBundle\Exception\MoreThenOneAttributeException;
use Dexodus\EntityFormBundle\Service\FieldGenerator\FieldGeneratorInterface;
use Dexodus\EntityFormBundle\Service\PathConstructor;
use Dexodus\EntityFormBundle\Service\ValidatorExtractor;
use Dexodus\EntityTableBundle\Attribute\ColumnAttributeInterface;
use Dexodus\EntityTableBundle\Attribute\EntityTableColumn as EntityTableColumnAttribute;
use Dexodus\EntityTableBundle\Dto\EntityTableColumn;
use Dexodus\TextCaseBundle\Enum\TextCaseEnum;
use Dexodus\TextCaseBundle\Service\WordsExtractor\WordsExtractor;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\PropertyInfo\PropertyAccessExtractorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

class ColumnGenerator
{
    public function __construct(
        private PropertyInfoExtractorInterface   $propertyInfoExtractor,
        private PropertyAccessExtractorInterface $propertyAccessExtractor,
        private PathConstructor                  $pathConstructor,
        private WordsExtractor                   $wordsExtractor,
    ) {
    }

    public function generate(string $objectClass): array
    {
        $reflectionClass = new ReflectionClass($objectClass);
        $propertyNames = [];

        foreach ($reflectionClass->getProperties() as $reflectionProperty) {
            $propertyNames[] = $reflectionProperty->getName();
        }

        $properties = $this->propertyInfoExtractor->getProperties($objectClass) ?? [];
        $columns = [];

        foreach ($properties as $property) {
            if (!in_array($property, $propertyNames)) {
                continue;
            }

            $reflectionProperty = new ReflectionProperty($objectClass, $property);
            $attributes = $reflectionProperty->getAttributes();
            /** @var FieldAttributeInterface[] $attributeInstances */
            $attributeInstances = [];

            foreach ($attributes as $attribute) {
                $attributeInstance = $attribute->newInstance();

                if (!$attributeInstance instanceof ColumnAttributeInterface) {
                    continue;
                }

                if (array_key_exists($attribute->getName(), $attributeInstances)) {
                    throw new MoreThenOneAttributeException($objectClass, $property, $attribute->getName());
                }

                $attributeInstances[$attribute->getName()] = $attributeInstance;
            }

            $entityTableColumnAttributes = $reflectionProperty->getAttributes(EntityTableColumnAttribute::class);

            if (!$this->propertyAccessExtractor->isReadable($objectClass, $property)) {
                continue;
            }

            if (count($entityTableColumnAttributes) > 1) {
                throw new MoreThenOneAttributeException($objectClass, $property, EntityTableColumnAttribute::class);
            }

            if (count($entityTableColumnAttributes) === 0) {
                continue;
            }

            /** @var EntityTableColumnAttribute $entityTableColumnAttribute */
            $entityTableColumnAttribute = $entityTableColumnAttributes[0]->newInstance();

            $columns[] = $this->getColumn($objectClass, $property, $entityTableColumnAttribute);
        }

        usort($columns, function (EntityTableColumn $a, EntityTableColumn $b) {
            if ($a->priority > $b->priority) {
                return -1;
            } elseif ($a->priority < $b->priority) {
                return 1;
            } else {
                return 0;
            }
        });

        return $columns;
    }

    private function getColumn(string $objectClass, string $property, EntityTableColumnAttribute $entityTableColumnAttribute): EntityTableColumn
    {
        $reflectionProperty = new ReflectionProperty($objectClass, $property);
        $priorityAttributes = $reflectionProperty->getAttributes(Priority::class);

        if (count($priorityAttributes) > 1) {
            throw new MoreThenOneAttributeException($objectClass, $property, EntityTableColumnAttribute::class);
        }

        $property = $entityTableColumnAttribute->path ?? $property;
        $propertyPath = $this->pathConstructor->getPath($objectClass, $property);

        $column = new EntityTableColumn();

        $column->title = $property;
        $column->dataKey = $property;
        $column->title = ucwords(implode(' ', $this->wordsExtractor->extract($property, TextCaseEnum::CAMEL_CASE)));
        $column->title = $entityTableColumnAttribute->title ?? $column->title;
        $column->priority = 0;

        $action = $entityTableColumnAttribute->getAction();

        if (is_null($action)) {
            $column->getDataAction = $this->pathConstructor->getPath($objectClass, $propertyPath, 'entity');
        } else {
            $column->getDataAction = $action;
        }

        if (!empty($priorityAttributes)) {
            /** @var Priority $priorityAttribute */
            $priorityAttribute = $priorityAttributes[0]->newInstance();
            $column->priority = $priorityAttribute->priority;
        }

        return $column;
    }
}
