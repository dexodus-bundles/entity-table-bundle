<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Exception;

use Exception;

class NotFoundFilterTypeException extends Exception
{
}
