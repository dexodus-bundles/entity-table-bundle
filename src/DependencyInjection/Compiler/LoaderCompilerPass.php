<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\DependencyInjection\Compiler;

use Dexodus\EntityTableBundle\Attribute\EntityTable as EntityTableAttribute;
use Dexodus\EntityTableBundle\Service\EntityTableLoaderInterface;
use Exception;
use ReflectionAttribute;
use ReflectionClass;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class LoaderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $loaderClass = $container->getParameter('entity-table.loader_class');

        if (!$container->hasDefinition($loaderClass)) {
            $container->setDefinition($loaderClass, new Definition($loaderClass));
        }

        $container->setAlias(EntityTableLoaderInterface::class, $loaderClass);
        $loader = $container->findDefinition(EntityTableLoaderInterface::class);

        foreach ($container->getParameter('entity-table.mapping') as $mapping) {
            $potentialEntityTablePaths = scandir($mapping['dir']);

            if ($potentialEntityTablePaths === false) {
                throw new Exception("Directory '{$mapping['dir']}' not found for load entity tables");
            }

            $this->scanDirectoryAndAddEntityTableToLoader($mapping['dir'], $mapping['prefix'], $loader);
        }
    }

    private function scanDirectoryAndAddEntityTableToLoader(string $directory, string $prefix, Definition $loader): void
    {
        $entityTables = [];

        foreach (scandir($directory) as $path) {
            if ($path === '.' || $path === '..') {
                continue;
            }

            $filepath = $directory . '/' . $path;
            $class = $prefix . '\\' . str_replace('.php', '', $path);

            if (!class_exists($class)) {
                if (is_dir($filepath)) {
                    $this->scanDirectoryAndAddEntityTableToLoader($filepath, $prefix . '\\' . $path, $loader);
                }

                continue;
            }

            $entityTableAttributes = $this->getEntityTableAttributes($class);

            if (count($entityTableAttributes) === 0) {
                continue;
            }

            $entityTables[] = $class;
        }

        $loader->addMethodCall('setEntityTables', [array_unique($entityTables)]);
    }

    /** @return ReflectionAttribute[] */
    private function getEntityTableAttributes(string $class): array
    {
        $reflectionClass = new ReflectionClass($class);

        return $reflectionClass->getAttributes(EntityTableAttribute::class);
    }
}
