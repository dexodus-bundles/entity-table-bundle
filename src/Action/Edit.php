<?php

declare(strict_types=1);

namespace Dexodus\EntityTableBundle\Action;

use Dexodus\EntityTableBundle\Enum\ActionStyleEnum;
use Dexodus\TitleBundle\Attribute\Title;

#[Title('Редактировать')]
readonly class Edit implements ActionInterface
{
    public function __construct(
        private ActionStyleEnum $style = ActionStyleEnum::Violet,
    ) {
    }

    public function configure(array $config): void
    {
    }

    public function onClick(): string
    {
        return 'routerPush("create?id=" + entity.id)';
    }

    public function getStyle(): ActionStyleEnum
    {
        return $this->style;
    }

    public function isVisible(): string
    {
        return 'true';
    }
}
